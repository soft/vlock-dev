#pragma once

// ROSA: localization
#include <libintl.h>
#include <locale.h>
#define _(string) gettext(string)